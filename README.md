Distributional Semantics meet Multi-Label Learning
===================================================================================
ExMLDS source code

- Vivek Gupta, Rahul Wadbude, Nagararjan Natararjan, Harish Karnick, Prateek Jain, Piyush Rai . Distributional Semantics meet Multi-Label Learning. AAAI 2019. ([AAAI 2019 Webpage]())

For more detail, please see [the paper](https://aaai.org/ojs/index.php/AAAI/article/view/4260).

Publications
------------

- Vivek Gupta, Rahul Wadbude, Nagararjan Natararjan, Harish Karnick, Prateek Jain, Piyush Rai . Distributional Semantics meet Multi-Label Learning. AAAI 2019. ([AAAI 2019 Webpage](https://aaai.org/ojs/index.php/AAAI/article/view/4260))

License
-------

Please make sure that you have read the license agreement in LICENSE.doc/pdf. Please do not install or use any code unless you agree to the terms of the license.


Cite
----
If you want to use this code, please kindly cite our paper

@inproceedings{gupta2019exmlds,
  title={Distributional Semantics meet Multi-Label Learning},
  author={Gupta, Vivek},
  booktitle={Proceedings of the AAAI 2019},
  year={2019},
  organization={AAAI}
}
