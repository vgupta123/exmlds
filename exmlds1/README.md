Distributional Semantics meet Multi-Label Learning
===================================================================================
ExMLDS source code

- Vivek Gupta, Rahul Wadbude, Nagararjan Natararjan, Harish Karnick, Prateek Jain, Piyush Rai . Distributional Semantics meet Multi-Label Learning. AAAI 2019. ([AAAI 2019 Webpage](https://aaai.org/ojs/index.php/AAAI/article/view/4260))

For more detail, please see [the paper](https://aaai.org/ojs/index.php/AAAI/article/view/4260).

Please make sure that you have read the license agreement in LICENSE.doc/pdf. Please do not install or use EXMLDS or SLEEC unless you agree to the terms of the license.

Publications
------------

- Vivek Gupta, Rahul Wadbude, Nagararjan Natararjan, Harish Karnick, Prateek Jain, Piyush Rai . Distributional Semantics meet Multi-Label Learning. AAAI 2019. ([AAAI 2019 Webpage](https://aaai.org/ojs/index.php/AAAI/article/view/4260))

- K. Bhatia, H. Jain, P. Kar, M. Varma, and P. Jain, Sparse Local Embeddings for Extreme Multi-label Classification, in NIPS, 2015. 

Cite
----
If you want to use this code, please kindly cite our paper

**The code  requires prior installation of libinear
  The windows 64 bit binary of 'train' is already included in the sleec_train folder
  To run on other platforms, compile liblinear and use the matlab train executable for linux in sleec_train
  For parallelization, we have used openmp library, just make sure that the compiler you are using
  is OpenMP compatible**
  
To compile exmlds1, run the following command on the Matlab prompt
> make_SLEEC

The following explains the matlab structure for the data, parameters and the output result

exmlds1 Data

	data    : structure containing the complete dataset
	data.X  : sparse n x d matrix containing train features
	data.Xt : sparse m x d matrix containing test features
	data.Y  : sparse n x L matrix containing train labels
	data.Yt : sparse m x L matrix containing test labels
exmlds1 Parameters

	params.num_learners : number of SLEEC learners in ensemble (default 10)
	params.num_clusters : Initial number of clusters (default 300)
	params.num_threads  : Number of threads for parallelization (default 32)
	params.SVP_neigh    : Number of nearest neighbours to be preserved (default 15)
	params.out_Dim      : embedding dimensions (default 50)
	params.w_thres      : 1-w_thresh is the sparsity of regressors w (default 0.7)
	params.sp_thresh    : 1-sp_thresh is the sparsity of embeddings (default 0.7)
	params.cost         : liblinear cost coefficient (default 0.1)
	params.NNtest       : number of nearest neighbours to consider while testing (default 10)
	params.normalize    : 1 for normalized data, 2 for unnormalized data (only for mediamill)
	params.fname        : filename for logging purposes
exmlds1 Result

	result.clusterCenters     : cluster centers for the different learners
	result.tim_clus           : time taken for clustering
	result.SVPModel           : model for different learners containing embeddings and regressors
	result.SVPtime_mat        : time taken for performing SVP for each learner
	result.regressiontime_mat : time taken for learning regressors
	result.precision          : overall precision accuracy
	result.predictAcc         : precision accuracy per test point
	result.predictLabels      : Top-k labels predicted per test point
	result.tim_test           : time taken for the testing procedure
	result.test_KNN           : kNN Matrix for the test points
Toy Example

The zip file containing the source code also include the BibTeX dataset as a toy example. The following are the instructions to run SLEEC on the BibTeX dataset

> controller

