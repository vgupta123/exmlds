w_thresh = SVPMLparams.w_thresh;
spParam = SVPMLparams.sp_thresh;

Yt = data.Yt; Y = data.Y;

if(SVPMLparams.AG == 2)
    XAG = XX;
    XTAG = XXT;
else
    XAG = data.X;
    XTAG = data.Xt;
end

normY = sqrt(sum((Y'.^2), 1)) + 1e-10;
Y = bsxfun(@rdivide, Y', normY);
Y = Y';
%Y = data.Y;
[n, l] = size(Y);
[nt, ~] = size(Yt);

%Set the parameters for svp
outDim = SVPMLparams.outDim;
params.tol=1e-3;
params.mxitr=SVPMLparams.mxitr;
params.verbosity=1;

%storing precisions (5xnumClusters) and numer of datapoints(numClustersx1)
numClusterAssigned = max(assign) + 1;
dpClusters = zeros(numClusterAssigned, 1);
embedTr = zeros(size(data.X,1), outDim);
svpEmbedCluster = {};
polyKernelAlpha = {};
svpTime = 0;
regressionTime = 0;

lambda = SVPMLparams.lambda;
fid = fopen(SVPMLparams.outfile, 'w');

Ytr = Y';
XAGtr = XAG';
dytr = data.Y';
XTAGtr = XTAG';

numThreads = SVPMLparams.numThreads;
%Form the svp embedding for each cluster obtained
for clusterIter = 0:max(assign)
    %Create separate dataset
    dp = find(assign == clusterIter);
    ds = Ytr(:, dp)';
    dsY = dytr(:, dp)';
    dsx = XAGtr(:, dp)';
    nc = size(dsx,1);
    dpClusters(clusterIter+1) = nc;
    numNeighbors = SVPMLparams.SVPneigh;
    outDim = SVPMLparams.outDim;
    numNeighbors = min(numNeighbors, nc);
    outDim  = min(outDim, nc);
    
    fprintf('\n Cluster : %d NumTrainPoints : %d outDim : %d\n', clusterIter, nc, outDim);
    fprintf(fid, '\n Cluster : %d NumTrainPoints : %d\n', clusterIter, nc);
    [Om, OmVal, neighborIdx] = findKNN_test(ds', numNeighbors, numThreads);
    str = 'dep.txt';
    str2 = 'dep2.txt';
    fpp=fopen(str,'w');
    for i=1:size(OmVal,1)
     for j=1:size(OmVal,2)
      fprintf('%d/%d\n',(i-1)*size(OmVal,2)+j,size(OmVal,2)*size(OmVal,1));
      for k=1:1
       fprintf(fpp,'%d %d\n',j,neighborIdx(i,j));
      end
     end
    end
    fclose(fpp);
    status = unix(strcat('shuf',32,str,32,'>',32,str2));
    status = unix(strcat('../../warahul-word2vecf-1b94252a58d4/count_and_filter -train',32, str2,32 ,'-cvocab cv -wvocab wv -min-count 0'));
    status = unix(strcat('../../warahul-word2vecf-1b94252a58d4/word2vecf -train',32, str2,32 ,'-wvocab wv -cvocab cv -output dim200vecs -size ',32,num2str(outDim),' -threads 40 -iters', 32,num2str(SGNSiter),32, '-negative',32,num2str(negative_sampling)));
    status = unix('python ../../warahul-word2vecf-1b94252a58d4/scripts/vecs2nps.py dim200vecs vecs');
    load('embedding.mat');
    Zc = wvecs;
    [W, alpha, mu] = computeW(dsx, Zc, 0.001, 0.01, 2, SVPMLparams.c);
    [W_I, W_J, W_lin] = find(W);
    W_sort = sort(abs(W_lin));
    w_idx = ceil(w_thresh*length(W_sort));
    if(w_idx==0)
        w_idx = 1;
    end
    W_lin(abs(W_lin)<W_sort(w_idx)) = 0;
    W = sparse(W_I, W_J, W_lin, size(XX, 2), outDim);
    
    polyKernelAlpha{clusterIter + 1} = W;
    Zct = full(dsx*W);
    sp_thresh_v = zeros(outDim, 1);

    for sp_i= 1:outDim
       [a, a_idx] = sort(abs(Zct(:, sp_i)));
       sp_thresh = a_idx(1:ceil(nc*spParam));
       Zct(sp_thresh, sp_i) = 0;
       sp_thresh_v(sp_i) = a(sp_thresh(end));
    end
    svpEmbedCluster{clusterIter +1} = Zct;
    
end
numNeighbors = SVPMLparams.SVPneigh;
outDim = SVPMLparams.outDim;
fclose(fid);
