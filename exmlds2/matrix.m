function [mat] = matrix(scoreMat, nt, k, nl)

mat=sparse(nt,nl);
for i=1:k
	for j=1:nt
		mat(j,scoreMat(i,j)+1)=6-i;
	end
end
