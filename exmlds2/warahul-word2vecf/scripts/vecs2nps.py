import numpy as np
from scipy import io as sio
import sys

fh=file(sys.argv[1])
foutname=sys.argv[2]
first=fh.next()
size=map(int,first.strip().split())

wvecs=np.zeros((size[0],size[1]),float)

vocab=[]
for i,line in enumerate(fh):
    line = line.strip().split()
    vocab.append(line[0])
    wvecs[int(line[0])-1,] = np.array(map(float,line[1:]))
sio.savemat('embedding.mat', {'wvecs':wvecs})
