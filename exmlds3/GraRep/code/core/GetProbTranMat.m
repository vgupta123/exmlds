function probTranMat = GetProbTranMat(Ak)

[p, q] = size(Ak);
assert(p==q, 'M must be a square matrix!');

%for more details, plz see our paper
fp = fopen('changing1.txt','a');
probTranMat = log(Ak ./ repmat(sum(Ak), p, 1)) - log(1/p);
fprintf(fp,'S\n%d\n',p*q);
p*q
fprintf(fp,'%d\n',nnz(probTranMat<0));
nnz(probTranMat<0)
probTranMat(probTranMat<0)=0;                   %positive
IdxNan = isnan(probTranMat);
nnz(IdxNan)
fprintf(fp,'%d\n',nnz(IdxNan));
probTranMat(IdxNan) = 0;
fclose(fp);
end
