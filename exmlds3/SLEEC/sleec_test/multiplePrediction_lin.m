function [result,predictAcc,predictLabels, KNN] = multiplePrediction_lin(data, assign_mat, clusterCenters, SVPModel, SVPMLparams, NNtest, T, numThreads)

if(SVPMLparams.AG == 2)
    XTAG = normalizeMatrix(data.Xt);
else
    XTAG = data.Xt;
end

XTAGtr = XTAG';
tData.Xt = XTAGtr;

numNeighPerClus = NNtest;
[n, d]= size(data.X);
[nt, l] = size(data.Yt);

XXT_t = tData.Xt;
KNN = zeros(nt, T*numNeighPerClus);
KNN2 = zeros(nt, T*5);
KNNVAL2 = zeros(nt, T*5);
for t = 1:T
   
   assign = assign_mat(t, :);
   numCluster = max(assign)+1;
   cc = clusterCenters{t};
   [tassign] = identifyClusterDP_FtSp_sparse(XXT_t, cc, numCluster);
   for clusIter = 0:max(assign)
       dp = find(assign == clusIter);
       dpt = find(tassign == clusIter);
       
       dpl = length(dp);
       
       W = SVPModel{t}.alpha2{clusIter+1};
       ztrain = SVPModel{t}.wvecs{clusIter+1};

       ztest = (XXT_t(:, dpt)'*W);
       ztest = full(ztest);
       numNeigh = min(numNeighPerClus, dpl);
       [KNNidx,KNNval] = findKNN_rf_ed(ztrain', ztest', 5, numThreads);
       if(numNeighPerClus > numNeigh)
        if((numNeighPerClus-numNeigh) < dpl)
            KNNidx = [KNNidx(1:(numNeighPerClus- numNeigh), :); KNNidx];
        else
            for my_i = 1:numNeighPerClus-numNeigh
                KNNidx = [KNNidx(1, :); KNNidx];
            end
        end
       end
       
       KNN2(dpt, (t-1)*5 + 1:t*5) =  KNNidx';
       KNNVAL2(dpt, (t-1)*5 + 1:t*5) =  KNNval';
   end  
end

labelscores2 = sparse(size(data.Yt,1),size(data.Yt,2));
for i=1:nt
	for j=1:5
		labelscores2(i,KNNidx(j,i))=KNNval(j,i);
	end
end
[precision_k] = evalPrecision(labelscores2,data.Yt,5)
result = precision_k;
end
