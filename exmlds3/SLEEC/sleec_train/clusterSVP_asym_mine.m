w_thresh = SVPMLparams.w_thresh;
spParam = SVPMLparams.sp_thresh;

Yt = data.Yt; Y = data.Y;

if(SVPMLparams.AG == 2)
    XAG = XX;
    XTAG = XXT;
else
    XAG = data.X;
    XTAG = data.Xt;
end

normY = sqrt(sum((Y'.^2), 1)) + 1e-10;
Y = bsxfun(@rdivide, Y', normY);
Y = Y';
%Y = data.Y;
[n, l] = size(Y);
[nt, ~] = size(Yt);

%Set the parameters for svp
outDim = SVPMLparams.outDim;
params.tol=1e-3;
params.mxitr=SVPMLparams.mxitr;
params.verbosity=1;

%storing precisions (5xnumClusters) and numer of datapoints(numClustersx1)
numClusterAssigned = max(assign) + 1;
dpClusters = zeros(numClusterAssigned, 1);
embedTr = zeros(size(data.X,1), outDim);
svpEmbedCluster = {};
svpEmbedwordvecs = {};
polyKernelAlpha = {};
polyKernelAlpha2 = {};
svpTime = 0;
regressionTime = 0;

lambda = SVPMLparams.lambda;
fid = fopen(SVPMLparams.outfile, 'w');

Ytr = Y';
XAGtr = XAG';
dytr = data.Y';
XTAGtr = XTAG';

numThreads = SVPMLparams.numThreads;
%Form the svp embedding for each cluster obtained
for clusterIter = 0:max(assign)
    %Create separate dataset
    dp = find(assign == clusterIter);
    ds = Ytr(:, dp)';
    dsY = dytr(:, dp)';
    
    normY = sqrt(sum((dsY.^2), 1)) + 1e-10;
    dsY = bsxfun(@rdivide, dsY, normY); 
    dsY = dsY';

    dsx = XAGtr(:, dp)';
    nc = size(dsx,1);
    dpClusters(clusterIter+1) = nc;
    numNeighbors = SVPMLparams.SVPneigh;
    outDim = SVPMLparams.outDim;
    numNeighbors = min(numNeighbors, nc);
    outDim  = min(outDim, nc);
    
    fprintf('\n Cluster : %d NumTrainPoints : %d outDim : %d\n', clusterIter, nc, outDim);
    fprintf(fid, '\n Cluster : %d NumTrainPoints : %d\n', clusterIter, nc);
    [Om, OmVal, neighborIdx] = findKNN_test(ds', numNeighbors, numThreads);
    
    fpp=fopen('dep1.contexts','w');
    for i=1:size(OmVal,1)
     for j=1:size(OmVal,2)
      fprintf('%d/%d\n',(i-1)*size(OmVal,2)+j,size(OmVal,2)*size(OmVal,1));
	if j~=neighborIdx(i,j)
       		fprintf(fpp,'d_%d d_%d\n',j,neighborIdx(i,j));
	end
     end
    end
    
    [I,J,K] = find(ds);
    for i=1:length(I)
	fprintf(fpp,'d_%d w_%d\n',I(i),J(i));
    end
 
    [Om, OmVal, neighborIdx] = findKNN_test(info', label_neigh, numThreads);
    for i=1:size(OmVal,1)
    for j=1:size(OmVal,2)
      fprintf('%d/%d\n',(i-1)*size(OmVal,2)+j,size(OmVal,2)*size(OmVal,1));
      if j~=neighborIdx(i,j)
		for k=1:10
	       fprintf(fpp,'w_%d w_%d\n',j,neighborIdx(i,j));
		end
      end
     end
    end
    fclose(fpp);
    status = unix('shuf dep1.contexts > dep3.contexts')
    status = unix('../../warahul-word2vecf-1b94252a58d4/count_and_filter -train dep3.contexts -cvocab cv -wvocab wv -min-count 0');
    status = unix(strcat('../../warahul-word2vecf-1b94252a58d4/word2vecf -train dep3.contexts -wvocab wv -cvocab cv -output dim200vecs -size ',32,num2str(outDim),' -threads 40 -iters 300 -negative 15'));
    status = unix(strcat('python ../../warahul-word2vecf-1b94252a58d4/scripts/vecs2nps.py dim200vecs vecs',32,num2str(size(ds,1)),32,num2str(size(ds,2))));
    load('embedding.mat');
    Zc = dvecs;
    [W, alpha, mu] = computeW(dsx, Zc, 0.001, 0.01, 2, SVPMLparams.c);
    [W_I, W_J, W_lin] = find(W);
    W_sort = sort(abs(W_lin));
    w_idx = ceil(w_thresh*length(W_sort));
    if(w_idx==0)
        w_idx = 1;
    end
    W_lin(abs(W_lin)<W_sort(w_idx)) = 0;
    W = sparse(W_I, W_J, W_lin, size(XX, 2), outDim);
    
    polyKernelAlpha{clusterIter + 1} = W;
    Zct = full(dsx*W);
    sp_thresh_v = zeros(outDim, 1);

    for sp_i= 1:outDim
       [a, a_idx] = sort(abs(Zct(:, sp_i)));
       sp_thresh = a_idx(1:ceil(nc*spParam));
       Zct(sp_thresh, sp_i) = 0;
       sp_thresh_v(sp_i) = a(sp_thresh(end));
    end
    svpEmbedCluster{clusterIter +1} = Zct;
    polyKernelAlpha{clusterIter + 1} = W;
    svpEmbedCluster{clusterIter +1} = Zct;
    [i,j,s]=find(ds);
    Mat2=zeros(size(ds,1),outDim);
    count = zeros(size(ds,1),1);
    for k=1:length(i)
        Mat2(i(k),:) = Mat2(i(k),:) + wvecs(j(k),:);
	count(i(k)) = count(i(k))+1;
    end
    for k=1:size(count)
      Mat2(k) = Mat2(k)/count(k); 
    end
    [W, alpha, mu] = computeW(dsx, Mat2, 0.001, 0.01, 3, SVPMLparams.c);
    
    [W_I, W_J, W_lin] = find(W);
    W_sort = sort(abs(W_lin));
    w_idx = ceil(w_thresh*length(W_sort));
    if(w_idx==0)
        w_idx = 1;
    end
    W_lin(abs(W_lin)<W_sort(w_idx)) = 0;
    W = sparse(W_I, W_J, W_lin, size(XX, 2), outDim);
    polyKernelAlpha2{clusterIter + 1} = W;
    svpEmbedwordvecs{clusterIter +1} = wvecs;
end
numNeighbors = SVPMLparams.SVPneigh;
outDim = SVPMLparams.outDim;
fclose(fid);
