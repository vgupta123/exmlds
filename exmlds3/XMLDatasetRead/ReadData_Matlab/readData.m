function [data] = readData(dname)

train_name = [dname, '_train.txt']
test_name = [dname, '_test.txt']

[ft_mat,lbl_mat] = read_data(train_name);
data.X = ft_mat';
data.Y = lbl_mat';

[ft_mat,lbl_mat] = read_data(test_name);
data.Xt = ft_mat';
data.Yt = lbl_mat';

end
