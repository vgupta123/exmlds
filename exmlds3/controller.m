clear;
cd('Toy_Example');
load('bibtex.mat');
normY = sqrt(sum((data.Y.^2), 1)) + 1e-10;
info = bsxfun(@rdivide, data.Y, normY);
info = info';
[I,J,K] = find(data.Y);
for i=1:length(I)
	if mod(i,5)~=0
		data.Y(I(i),J(i)) = 0;
	end
end
bibtexParams;
cd('..');
[result]=SLEEC(data,info,params);
