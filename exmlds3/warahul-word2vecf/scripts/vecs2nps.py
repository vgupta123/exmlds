import numpy as np
from scipy import io as sio
import sys

fh=file(sys.argv[1])
foutname=sys.argv[2]
first=fh.next()
size=map(int,first.strip().split())

dvecs = np.zeros((int(sys.argv[3]),size[1]),float)
wvecs = np.zeros((int(sys.argv[4]),size[1]),float)
vocab=[]
for i,line in enumerate(fh):
    line = line.strip().split()
    vocab.append(line[0])
    list1 = line[0].split('_')
    if list1[0]=='d':
	dvecs[int(list1[1])-1,] = np.array(map(float,line[1:]))
    else:
	wvecs[int(list1[1])-1,] = np.array(map(float,line[1:]))
sio.savemat('embedding.mat', {'wvecs':wvecs,'dvecs':dvecs})
