import sys
import subprocess


parameterDict = {
	'emb_size' : [50, 150],
	'num_nn': [10, 20],
	'emb_iter': [10, 20],
}

def runWithSweep(i, emb_size, num_nn, emb_iter):
	f = open('temp.json')
	fileStr = f.read()
 	fileStr = fileStr.replace('XXXX1', "%d" % emb_size)
	fileStr = fileStr.replace('XXXX2', "%d" % num_nn)
	fileStr = fileStr.replace('XXXX3', "%d" % emb_iter)
	f.close()
	g = open('temp_out.json', 'w')
	g.write(fileStr)
	g.close()
	line = 'RUN: %d emb_size: %d, num_nn: %d, emb_iter: %d' %(i, emb_size, num_nn, emb_iter)
	print(line)
	return line



def executeSubProcess(cmd, header):
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	fileStr = header +'\n'
	for line in p.stdout:
		fileStr += line
	fileStr += '\n'
	f = open('output', 'a+')
	f.write(fileStr)
	f.close()
	p.wait()



i = 0
for emb_size in parameterDict['emb_size']:
	for num_nn in parameterDict['num_nn']:
		for emb_iter in parameterDict['emb_iter']:
			line = runWithSweep(i, emb_size, num_nn, emb_iter)
			# Train
			cmd = ['src/annexml', 'train', 'temp_out.json', 'num_thread=20']
			executeSubProcess(cmd, 'Train: ' + line)
			# Predict
			cmd = ['src/annexml', 'predict', 'temp_out.json', 'num_thread=20']
			executeSubProcess(cmd, 'Test: ' + line)
			# Evaluate
			cmd = ['mv', 'annexml-result-example.txt', 'output/result_%d_%d_%d' % (emb_size, num_nn, emb_iter)]
			executeSubProcess(cmd, 'Predict:' + line)
			i += 1




